//
//  CommentsViewController.swift
//  Arborlab
//
//  Created by Ming Fang on 5/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class CommentsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ImagesProtocol{

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var commentsTextView: UITextView!
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var selectedItems = [String]()
    var images = [UIImage]()
    var auditResultData:AuditResultsData!
    var imageDic = [URL]()
    var username = ""

    var uploadImage = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserInfo()
        
        if let value = Shared.shared.auditResultData {
            auditResultData = value as AuditResultsData
        }
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width - 34
        screenHeight = screenSize.height
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(red:0.933, green:0.933, blue:0.933, alpha:1)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth/2, height: screenWidth/2)
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        collectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.reloadData()
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {}
    
    @IBAction func doneBtn(_ sender: Any) {
        uploadImageByNum()
    }
    @IBAction func imageToCommentsBtn(_ sender: Any) {
        if images.count < 2 {
            getImagesFromUploader()
        } else {
            let alertController = UIAlertController(title: "Images Selection", message: "Do you want to reselct images?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                self.images.removeAll()
                self.imageDic.removeAll()
                self.getImagesFromUploader()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func getUserInfo() {
        if let userId = UserDefaults.standard.string(forKey: "userId") {
            let realm = try! Realm()
            let userFilter = NSPredicate(format: "id = %d", Int(userId)!)
            let userRealm = realm.objects(User.self).filter(userFilter)
            for item in userRealm {
                username = item.username
            }
        }
    }
    
    func getImagesFromUploader() {
        Shared.shared.imageNumber = images.count
        
        let destinationStoryboard = storyboard?.instantiateViewController(withIdentifier: "ImageUploadVC") as! ImageUploaderVC
        let navController = UINavigationController(rootViewController: destinationStoryboard)
        destinationStoryboard.delegate = self
        Shared.shared.imageMode = "imageToComments"
        present(navController, animated: true, completion: nil)
    }
    
    func getImage(image: UIImage, imagePath: URL) {
        images.append(image)
        imageDic.append(imagePath)
    }
    
    func uploadImageByNum() {
        if images.count <= 2 {
            if images.count == 1 {
                uploadImageToServer(imagePath: imageDic[0], imageNum: "1")
            }
            if images.count == 2 {
                uploadImageToServer(imagePath: imageDic[1], imageNum: "2")
            }
            dismiss(animated: true, completion: nil)
        }
    }
    
    func uploadImageToServer(imagePath: URL, imageNum: String) {
        let api_token:String = UserDefaults.standard.string(forKey: "api_token")!
        let parameters = ["username" : username,
                          "api_token": api_token,
                          "selectedItems" : username,
                          "comments" : username,
                          "pakename" : auditResultData.parkName,
                          "kpi_index": auditResultData.index,
                          "imageNum": imageNum,
                          "mode": "comments"]
        print(parameters)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(imagePath, withName: "image")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
        },
            to: Variables.userPostUrl,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response.result)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageUploaderCell", for: indexPath) as! ImageUploaderCell
        
        let uploadImageCount = images.count
        
        if uploadImageCount > 0 {
            cell.commentsCellImages.image = images[indexPath.row]
        } else {
            cell.commentsCellImages.image = #imageLiteral(resourceName: "AddImage")
        }
        
        if cell.commentsCellImages.image == UIImage(named: "AddImage") {
            cell.commentsCellImages.image = uploadImage
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageUploaderCell", for: indexPath) as! ImageUploaderCell

        if cell.commentsCellImages.image == UIImage(named: "AddImage") {
            let detinationController = storyboard?.instantiateViewController(withIdentifier: "ImageUploadVC") as! ImageUploaderVC
            let destinationNav = UINavigationController(rootViewController: detinationController)
            present(destinationNav, animated: true, completion: nil)
        }
    }
}

extension CommentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentsTVC", for: indexPath) as! CommentsTVC
        
        let selectedItem = UILabel(frame: CGRect(x: 5, y: 3, width: self.view.bounds.width - 38, height: 20))
        selectedItem.font = UIFont.systemFont(ofSize: 16)
        selectedItem.text = selectedItems[indexPath.row]
        
        cell.addSubview(selectedItem)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedItems.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
}

class CommentsTVC: UITableViewCell {
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        
        let whileSpacingView: UIView = UIView(frame: CGRect(x: 2, y: 2, width: UIScreen.main.bounds.width - 38, height: 22))
        
        whileSpacingView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whileSpacingView.layer.masksToBounds = false
        whileSpacingView.layer.cornerRadius = 2.0
        whileSpacingView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whileSpacingView.layer.shadowOpacity = 0.2
        
        self.contentView.addSubview(whileSpacingView)
        self.contentView.sendSubview(toBack: whileSpacingView)
    }
}
