//
//  ImagePageTwoCVC.swift
//  Arborlab
//
//  Created by Freedom on 30/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire


class ImagePageTwoCVC: UICollectionViewController, UICollectionViewDelegateFlowLayout{

    var auditResultData:AuditResultsData!
    let notificationMsg = NSNotification.Name(rawValue: "currentPageChanged")
    let pageViewMsg = NSNotification.Name(rawValue: "aaImages")
    var imageOneUrl = ""
    var imageTwoUrl = ""
    var receivedImages = [UIImage]()
    var imageCount = 0
    var imageDic = [URL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.imagesCount = receivedImages.count
        
        //setup image
        if let value = Shared.shared.auditResultData {
            auditResultData = value
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ImagePageTwoCVC.imageDeliverNotification(notification:)), name: pageViewMsg, object: nil)

        //not being used right now
//        imageOneUrl = Variables.imageUrl + FetchData.spaceRemove(originalUrl: auditResultData.aaPhoto1 as String)
//        imageTwoUrl = Variables.imageUrl + FetchData.spaceRemove(originalUrl: auditResultData.aaPhoto2 as String)
    }
    
    func imageDeliverNotification(notification: NSNotification) {
        let imageUrl:URL = notification.object as! URL

        //add image to collection view
        let data = NSData(contentsOf: imageUrl)
        let recivedImage: UIImage = UIImage(data: data as! Data)!
        
        if let status = Shared.shared.aaImageRemove {
            if !status {
                imageDic.removeAll()
                receivedImages.removeAll()
            }
        }
        
        receivedImages.append(recivedImage)
        imageDic.append(imageUrl)
        Shared.shared.imageUrlTwo = imageDic
        Shared.shared.imagesCount = imageDic.count
        self.collectionView?.reloadData()
    }
    
//    func isUrlValid(urlString : String) ->Bool{
//        let lastFourLetters = urlString.substring(from: urlString.index(urlString.endIndex, offsetBy: -4))
//        if lastFourLetters == ".jpg" || lastFourLetters == ".png" {
//            return true
//        }
//        return false
//    }

    override func viewWillAppear(_ animated: Bool) {

        NotificationCenter.default.post(name: notificationMsg, object: 2)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return receivedImages.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "aaImageCell", for: indexPath) as! ImagePageOneCell
        
        let aaImagesCount = receivedImages.count
        
        if aaImagesCount > 0 {
            cell.imageView.image = receivedImages[indexPath.row]
        } else {
            cell.imageView.image = #imageLiteral(resourceName: "AddImage")
        }
        
//        if indexPath.row == 0 {
//            cell.imageView.af_setImage(withURL: URL(string: imageOneUrl)!, placeholderImage: #imageLiteral(resourceName: "AddImage"), filter: nil, progress: { (progress) in
//                let progressBar = UIProgressView(frame: CGRect(x: 0, y: self.view.bounds.height - 2, width: self.view.bounds.width, height: 2))
//                progressBar.progress = Float(progress.fractionCompleted)
//                self.view.addSubview(progressBar)
//            }, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: true, completion: nil)
//        }
//        
//        if indexPath.row == 1 {
//            cell.imageView.af_setImage(withURL: URL(string: imageTwoUrl)!, placeholderImage: #imageLiteral(resourceName: "AddImage"), filter: nil, progress: { (progress) in
//                let progressBar = UIProgressView(frame: CGRect(x: 0, y: self.view.bounds.height - 2, width: self.view.bounds.width, height: 2))
//                progressBar.progress = Float(progress.fractionCompleted)
//                self.view.addSubview(progressBar)
//            }, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: true, completion: nil)
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width/2 - 2.5
        
        return CGSize(width: screenWidth, height: screenWidth)
    }
    
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "aaImageCell", for: indexPath) as! ImagePageOneCell
//        if cell.imageView.image == UIImage(named: "AddImage") {
//            let detinationController = storyboard?.instantiateViewController(withIdentifier: "ImageUploadVC") as! ImageUploaderVC
//            let destinationNav = UINavigationController(rootViewController: detinationController)
//            Shared.shared.imageMode = "uploadImageFromAA"
//            present(destinationNav, animated: true, completion: nil)
//        }
//    }
}
