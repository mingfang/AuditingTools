//
//  Models.swift
//  Arborlab
//
//  Created by Ming Fang on 18/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

protocol Meta {
    static func url() ->String
}

class ContractorResults: Object, Mappable, Meta{
    
    dynamic var unid = 0
    dynamic var id = ""
    dynamic var date = ""
    dynamic var asset = ""
    dynamic var sorcode = ""
    dynamic var sorgrouping = ""
    dynamic var tracker = ""
    
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "unid"
    }
    
    /// This function is where all variable mappings should occur. It is executed by Mapper during the mapping (serialization and deserialization) process.
    public func mapping(map: Map) {
        
        id    <- map["audit_match"]
        unid    <- map["uni_id"]
        date    <- map["actual_finish_date"]
        asset <- map["sort_field"]
        sorcode <- map["i_sor_code"]
        sorgrouping <- map["SOR_Grouping"]
        tracker <- map["aa_tracking"]
    }
    
    internal static func url() -> String {
        return Variables.contractorUrl
    }
    
}

class AuditResults: Object, Mappable, Meta{
    
    dynamic var id = 0
    dynamic var index = ""
    dynamic var parkname = ""
    dynamic var sorgrouping = ""
    dynamic var sorcode = ""
    dynamic var arbStatus = 0
    dynamic var date = ""
    dynamic var modifiedDate = ""
    dynamic var comments = ""
    dynamic var aaStatus = 0
    dynamic var assessment = ""
    dynamic var matchId = ""
    dynamic var photo1 = ""
    dynamic var photo2 = ""
    dynamic var aaPhoto1 = ""
    dynamic var aaPhoto2 = ""
    dynamic var tracker = ""
    
    
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    public func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(AuditResults.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    /// This function is where all variable mappings should occur. It is executed by Mapper during the mapping (serialization and deserialization) process.
    public func mapping(map: Map) {
        id    <- map["uni_id"]
        matchId <- map["audit_match_id"]
        date    <- map["date"]
        modifiedDate <- map["audit_date"]
        index    <- map["kpi_index"]
        parkname <- map["park_name"]
        sorcode <- map["sor_code"]
        sorgrouping <- map["sor_grouping"]
        aaStatus <- map["aa_compliance"]
        arbStatus <- map["compliance"]
        comments <- map["comments"]
        assessment <- map["assessment"]
        photo1 <- map["photo_1_properties"]
        photo2 <- map["photo_2_properties"]
        aaPhoto1 <- map["photo_1_name"]
        aaPhoto2 <- map["photo_1_name"]
    }
    
    internal static func url() -> String {
        return Variables.auditUrl
    }
}

class TempRealm: Object{
    
    dynamic var id = 0
    dynamic var index = ""
    dynamic var parkname = ""
    dynamic var sorgrouping = ""
    dynamic var sorcode = ""
    dynamic var arbStatus = 0
    dynamic var date = ""
    dynamic var modifiedDate = ""
    dynamic var comments = ""
    dynamic var aaStatus = 0
    dynamic var assessment = ""
    dynamic var matchId = ""
    dynamic var photo1 = ""
    dynamic var photo2 = ""
    dynamic var aaPhoto1 = ""
    dynamic var aaPhoto2 = ""
    dynamic var tracker = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    public func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(TempRealm.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
}

class User: Object, Mappable, Meta {
    
    dynamic var id = 0
    dynamic var username = ""
    dynamic var api_token = ""
    dynamic var email = ""
    dynamic var created_at = ""
    dynamic var updated_at = ""
    dynamic var today = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    public func mapping(map: Map) {
        id    <- map["id"]
        username <- map["username"]
        api_token    <- map["api_token"]
        email    <- map["email"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        
    }
    internal static func url() -> String {
        return Variables.userUrl
    }
}

class Questions: Object, Mappable, Meta {
    dynamic var id = 0
    dynamic var title = ""
    dynamic var index = ""
    dynamic var subtitle = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    public func mapping(map: Map) {
        id    <- map["hs_id"]
        index    <- map["index"]
        title <- map["description"]
        subtitle   <- map["details"]
        
    }
    internal static func url() -> String {
        return Variables.questionsUrl
    }
}

class HealthSafetyHistory: Object, Mappable, Meta {
    dynamic var id = 0
    dynamic var kpi_index = ""
    dynamic var username = ""
    dynamic var parkname = ""
    dynamic var photo1 = ""
    dynamic var photo2 = ""
    dynamic var comments = ""
    dynamic var createDate = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    public func mapping(map: Map) {
        id    <- map["his_id"]
        kpi_index    <- map["kpi_index"]
        username <- map["username"]
        parkname <- map["parkname"]
        photo1   <- map["photo_1"]
        photo2   <- map["photo_1"]
        createDate   <- map["create_date"]
        comments   <- map["comments"]
        
    }
    internal static func url() -> String {
        return Variables.healthSafetyUrl
    }
}
