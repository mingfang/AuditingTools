//
//  ImageUploaderVC.swift
//  Arborlab
//
//  Created by Freedom on 30/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol ImagesProtocol {
    @objc optional func getImage(image: UIImage, imagePath: URL)
}

class ImageUploaderVC: UIViewController {
    let pageViewMsg = NSNotification.Name(rawValue: "aaImages")
    var scrollView : UIScrollView!
    var imageView = UIImageView(image: UIImage(named: "addImage"))
    var image : UIImage!
    var leftOneItem: UIBarButtonItem!
    var localPath: URL?
    var auditResultData:AuditResultsData!
    var imageMode: String!
    var delegate: ImagesProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSetup()
        
        if let value = Shared.shared.auditResultData {
            auditResultData = value as AuditResultsData
        }
        
        //imageMode : imageToComments (not being used right now)
        if let value = Shared.shared.imageMode {
            imageMode = value
        }
        
        leftOneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(self.dismissView))
        self.navigationItem.setLeftBarButtonItems([leftOneItem], animated: true)
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {}
    @IBAction func cameraBtn(_ sender: Any) {
        openCamera()
    }
    @IBAction func libraryBtn(_ sender: Any) {
        pickImage()
    }
    
    private func initSetup() {
        scrollView = UIScrollView(frame: view.bounds)
        self.view.addSubview(scrollView)
        
        let imageView = UIImageView(frame: view.bounds)
        scrollView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 3
        scrollView.delegate = self
        scrollView.isScrollEnabled = false
        print(scrollView.zoomScale)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(ImageViewerVC.handleDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(doubleTap)
    }
    
    func recenterImage(_ scrollView: UIScrollView, imageView:UIImageView) {
        let offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width) ? (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0
        let offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height) ? (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0
        imageView.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
        
    }
    
    func handleDoubleTap(_ recognizer: UITapGestureRecognizer) {
        let imageview = recognizer.view as! UIImageView
        let scrollview = imageview.superview as! UIScrollView
        
        if scrollview.zoomScale > scrollview.minimumZoomScale {
            scrollview.setZoomScale(scrollview.minimumZoomScale, animated: true)
        } else if scrollview.zoomScale <= scrollview.minimumZoomScale{
            scrollview.setZoomScale(scrollview.maximumZoomScale, animated: true)
        }
    }
    
    //Image Selection
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func pickImage(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

}

extension ImageUploaderVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        let imageview = scrollView.subviews.first as! UIImageView
        imageview.image = pickedImage
        
        let documentDirectory: NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
        
        var imageName = "Photo.jpg"
        if let imageNumber = Shared.shared.imageNumber {
            if imageNumber == 0 {
                imageName = "Photo1.jpg"
            }
            if imageNumber == 1 {
                imageName = "Photo2.jpg"
            }
        }
        
        let imagePath = documentDirectory.appendingPathComponent(imageName)
        
        let imageUrl = URL(fileURLWithPath: imagePath)
        if let data = UIImageJPEGRepresentation(pickedImage, 80) {
            try! data.write(to: imageUrl, options: .atomic)
        }
        
        localPath = imageUrl
        
        if imageview.image != nil {
            let leftItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(self.sendImageToViewController))

            self.navigationItem.setLeftBarButtonItems([leftOneItem,leftItem], animated: true)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func sendImageToViewController() {
        guard let path = localPath else {
            return
        }
        
        Shared.shared.aaImageUrl = localPath
        
        let data = NSData(contentsOf: path)
        let delegateImage: UIImage = UIImage(data: data as! Data)!
        delegate?.getImage!(image: delegateImage, imagePath: localPath!)
        
        NotificationCenter.default.post(name: pageViewMsg, object: localPath)
        dismissView()
    }
    
    func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ImageUploaderVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        for imageView in scrollView.subviews {
            if imageView is UIImageView {
                return imageView
            }
        }
        return nil
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageView = scrollView.subviews.first as! UIImageView
        self.recenterImage(scrollView, imageView: imageView)
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scrollView.minimumZoomScale >= scale {
            scrollView.setZoomScale(1.0, animated: true)
        } else if scrollView.maximumZoomScale <= scale {
            scrollView.setZoomScale(3.0, animated: true)
        }
    }
}
