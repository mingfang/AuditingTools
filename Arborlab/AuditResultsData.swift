//
//  AuditData.swift
//  Arborlab
//
//  Created by Ming Fang on 21/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import Foundation
class AuditResultsData {
    var auditID:Int
    var parkName:String
    var auditDate:String
    var modifiedDate:String
    var sorGroup:String
    var sorCode:String
    var arbStatus:Int
    var aaStatus:Int
    var comments:String
    var assessment:String
    var index:String
    var matchid:String
    var photo1:String
    var photo2:String
    var aaPhoto1:String
    var aaPhoto2:String
    var id:Int
    var tracker:String
    
    init?(auditID:Int, parkName:String, auditDate:String, modifiedDate:String, sorGroup:String, sorCode:String, arbStatus:Int, aaStatus:Int, comments:String, assessment:String, index:String, matchid:String, photo1:String, photo2:String, aaPhoto1:String, aaPhoto2:String, id:Int, tracker:String) {
        self.auditID = auditID
        self.auditDate = auditDate
        self.modifiedDate = modifiedDate
        self.parkName = parkName
        self.sorCode = sorCode
        self.sorGroup = sorGroup
        self.aaStatus = aaStatus
        self.arbStatus = arbStatus
        self.comments = comments
        self.assessment = assessment
        self.index = index
        self.matchid = matchid
        self.photo1 = photo1
        self.photo2 = photo2
        self.aaPhoto1 = photo2
        self.aaPhoto2 = photo2
        self.id = id
        self.tracker = tracker
        

    }
}
