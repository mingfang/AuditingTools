//
//  SafetyRecordsTVC.swift
//  Arborlab
//
//  Created by Freedom on 2017/2/22.
//  Copyright © 2017年 Freedom. All rights reserved.
//

import UIKit
import RealmSwift

class SafetyRecordsTVC: UITableViewController {

    var parkname = ""
    var kpiIndex = ""
    var date = ""
    var comments = ""
    var numberOfRows = 0
    var numberOfImages = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRecordsFromRealm()
    }
    
    private func tableViewSetup() {
        tableView.rowHeight = 200
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    private func getRecordsFromRealm() {
        let realm = try! Realm()
        let realmRecords = realm.objects(HealthSafetyHistory.self)
        for item in realmRecords {
            parkname = item.parkname
            kpiIndex = item.kpi_index
            date = item.createDate.substring(to: item.createDate.index(item.createDate.startIndex, offsetBy: 10))
            if item.comments == "" {
                comments = "No comments"
            } else {
                comments = item.comments
            }
            var realmNumberOfImages = 0
            let lastElement1 = item.photo1.substring(from: item.photo1.index(item.photo1.endIndex, offsetBy: -1))
            if lastElement1 != "?" {
                realmNumberOfImages = numberOfImages + 1
            }
            let lastElement2 = item.photo1.substring(from: item.photo2.index(item.photo2.endIndex, offsetBy: -1))
            if lastElement2 != "?" {
                realmNumberOfImages = numberOfImages + 1
            }
            numberOfImages = realmNumberOfImages
        }
        numberOfRows = realmRecords.count
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SafetyRecordsCell", for: indexPath) as! SafetyRecordsCell

        cell.parkNameLabel.text = parkname
        cell.commentsLabel.text = comments
        cell.dateLabel.text = date
        cell.kpiIndexLabel.text = kpiIndex
        cell.imageNumber.text = String(numberOfImages) + " Images"
        return cell
    }
}

class SafetyRecordsCell: UITableViewCell {
    @IBOutlet weak var parkNameLabel: UILabel!
    @IBOutlet weak var kpiIndexLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageNumber: UILabel!
    
    override func awakeFromNib() {
        tableCellSetup()
    }
    
    private func tableCellSetup() {
        let whiteSpacingView: UIView = UIView(frame: CGRect(x: 4, y: 4, width: self.bounds.width - 8, height: self.bounds.height - 8))
        whiteSpacingView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1,1,1,0.9])
        whiteSpacingView.layer.masksToBounds = false
        whiteSpacingView.layer.cornerRadius = 2
        whiteSpacingView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteSpacingView.layer.shadowOpacity = 0.2
        self.contentView.addSubview(whiteSpacingView)
        self.contentView.sendSubview(toBack: whiteSpacingView)
    }
}
