//
//  VariableClass.swift
//  Arborlab
//
//  Created by Freedom on 25/01/17.
//  Copyright © 2017 Freedom. All rights reserved.
//

import UIKit

struct Variables {
    
    static let postUrl:String = "https://lumen.fmfreedom.com/api/post/"
    static let loginUrl:String = "https://lumen.fmfreedom.com/api/login"
    static let contractorUrl:String = "https://lumen.fmfreedom.com/api/contractor"
    static let uploadUrl:String = "https://lumen.fmfreedom.com/api/upload"
    //https://lumen.fmfreedom.com/api/aaimgupload/MATCHID
    static let aaUploadUrl:String = "https://lumen.fmfreedom.com/api/aaimgupload/"
    static let auditUrl:String = "https://lumen.fmfreedom.com/api/audit"
    static let userUrl:String = "https://lumen.fmfreedom.com/api/user"
    static let userPostUrl:String = "https://lumen.fmfreedom.com/api/user/post"
    static let registerUrl:String = "https://lumen.fmfreedom.com/api/register"
    static let questionsUrl:String = "https://lumen.fmfreedom.com/api/questions"
    static let healthSafetyUrl:String = "https://lumen.fmfreedom.com/api/healthsafety"
    static let imageUrl:String = "http://nas.fmfreedom.com/Arborlab/lumen5.2/Audit_Auditor_Photo/"

    struct Colors {
        static let primaryColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        static let darkColor = UIColor(red:0.227,  green:0.243,  blue:0.184, alpha:1)
        static let lightColor = UIColor(red:0.949,  green:0.961,  blue:0.933, alpha:1)
        static let passColor = UIColor(red:0.580,  green:0.792,  blue:0.271, alpha:1)
        static let failColor = UIColor(red:0.894,  green:0.263,  blue:0.278, alpha:1)
        static let superLightColor = UIColor(red:0.976,  green:0.976,  blue:0.976, alpha:1)
        static let cellBackGreenColor = UIColor(red:0.957,  green:1,  blue:0.847, alpha:1)
        static let cellBackRedColor = UIColor(red:1,  green:0.859,  blue:0.859, alpha:1)
    }
}
