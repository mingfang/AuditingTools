//
//  WorksCell.swift
//  Arborlab
//
//  Created by Ming Fang on 16/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit

class WorksCell: UITableViewCell {
    @IBOutlet weak var parkNameLabel: UILabel!
    @IBOutlet weak var sorgroupLabel: UILabel!
    @IBOutlet weak var sorcodeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        setBackground()
    }
    
    func setBackground() {
        let whileSpacingView: UIView = UIView(frame: CGRect(x: 5, y: 4, width: UIScreen.main.bounds.width - 10, height: 82))
        
        whileSpacingView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whileSpacingView.layer.masksToBounds = false
        whileSpacingView.layer.cornerRadius = 2.0
        whileSpacingView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whileSpacingView.layer.shadowOpacity = 0.2
        
        self.contentView.addSubview(whileSpacingView)
        self.contentView.sendSubview(toBack: whileSpacingView)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
