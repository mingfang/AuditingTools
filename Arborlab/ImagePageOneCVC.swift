//
//  ImagePageOneCVC.swift
//  Arborlab
//
//  Created by Freedom on 28/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import MBProgressHUD

class ImagePageOneCVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var auditResultData:AuditResultsData!
    let domainUrl = "http://203.171.55.87/Gatekeeper-DCC/"
    let notificationMsg = NSNotification.Name(rawValue: "currentPageChanged")
    var imageOneUrl = ""
    var imageTwoUrl = ""
    var imageCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup image
        if let value = Shared.shared.auditResultData {
            auditResultData = value
        }
        imageOneUrl = domainUrl + FetchData.spaceRemove(originalUrl: auditResultData.photo1 as String)
        imageTwoUrl = domainUrl + FetchData.spaceRemove(originalUrl: auditResultData.photo2 as String)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageCount = 0
        
        if isUrlValid(urlString: imageOneUrl) {
            imageCount += 1
        }
        
        if isUrlValid(urlString: imageTwoUrl) {
            imageCount += 1
        }
        
        self.collectionView?.reloadData()
        NotificationCenter.default.post(name: notificationMsg, object: 1)
    }
    
    func isUrlValid(urlString : String) ->Bool{
        
        let lastFourLetters = urlString.substring(from: urlString.index(urlString.endIndex, offsetBy: -4))
        if lastFourLetters == ".jpg" || lastFourLetters == ".png" {
            return true
        }
        return false
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImagePageOneCell
        
        if indexPath.row == 0 {
            cell.imageView.af_setImage(withURL: URL(string: imageOneUrl)!, placeholderImage: #imageLiteral(resourceName: "AddImage"), filter: nil, progress: { (progress) in
                let progressBar = UIProgressView(frame: CGRect(x: 0, y: self.view.bounds.height - 2, width: self.view.bounds.width, height: 2))
                progressBar.progress = Float(progress.fractionCompleted)
                self.view.addSubview(progressBar)
            }, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: true, completion: nil)
        }
        
        if indexPath.row == 1 {
            cell.imageView.af_setImage(withURL: URL(string: imageTwoUrl)!, placeholderImage: #imageLiteral(resourceName: "AddImage"), filter: nil, progress: { (progress) in
                let progressBar = UIProgressView(frame: CGRect(x: 0, y: self.view.bounds.height - 2, width: self.view.bounds.width, height: 2))
                progressBar.progress = Float(progress.fractionCompleted)
                self.view.addSubview(progressBar)
            }, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: true, completion: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width/2 - 2.5
        
        return CGSize(width: screenWidth, height: screenWidth)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageViewerVC: ImageViewerVC = storyboard?.instantiateViewController(withIdentifier: "ImageViewer") as! ImageViewerVC
        let imageViewNav = UINavigationController(rootViewController: imageViewerVC)
        
        if indexPath.row == 0 || indexPath.row == 1 {
            Shared.shared.imageNumber = indexPath.row
            Shared.shared.imageUrl = [imageOneUrl,imageTwoUrl]
            Shared.shared.imageMode = "Viewer"
            present(imageViewNav, animated: true, completion: nil)
        }
    }
}
