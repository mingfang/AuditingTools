//
//  QuestionListVC.swift
//  Arborlab
//
//  Created by Ming Fang on 13/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import RealmSwift

class QuestionListVC: UITableViewController {
    var isEnable = false
    var questionTitle = [String]()
    var questionDetail = [[String]]()
    var selectedCells = [String]()

    
    @IBAction func commentBtn(_ sender: Any) {
        
    }
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectBtn(_ sender: UIBarButtonItem) {
        isEnable = !isEnable
        if isEnable {
            sender.title = "Cancel"
        } else {
            sender.title = "Select"
        }
        
        self.navigationController?.isToolbarHidden = !isEnable
        self.navigationController?.isToolbarHidden = !isEnable
        self.tableView.allowsMultipleSelectionDuringEditing = isEnable
        self.tableView.setEditing(isEnable, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getQuestions()
        
        //tableview layout setup
        self.navigationController?.isToolbarHidden = true
        tableView.register(UINib(nibName: "QuestionListSecHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "QuestionListSecHeader")
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 30
        tableView.estimatedSectionHeaderHeight = 30
    }
    
    func getQuestions() {
        let realm = try! Realm()
        let questionResults = realm.objects(Questions.self)
        
        for item in questionResults {
            let title = String(item.id) + " ." + item.title
            let subtitle = String(item.index) + " ." + item.subtitle
            questionTitle.append(title)
            questionDetail.append([subtitle])
        }
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return questionTitle.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionDetail[section].count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionListTVC
        cell.questionDetials.text = self.questionDetail[indexPath.section][indexPath.row]
        cell.questionDetials.lineBreakMode = .byWordWrapping
        cell.questionDetials.numberOfLines = 0
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "QuestionListSecHeader") as! QuestionListSecHeader
        
        //setup text to the table header
        headerView.headerText.text = questionTitle[section]
        headerView.headerText.lineBreakMode = .byWordWrapping
        headerView.headerText.numberOfLines = 0
        
        //setup the background of the header
        //swift3 need setup the background with content view
        //(Instead creating UIView programmatically)
        let backgroundView = UIView(frame: headerView.bounds)
        backgroundView.backgroundColor = Variables.Colors.superLightColor
        headerView.backgroundView = backgroundView
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCells.append(questionDetail[indexPath.section].first!)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedCells.remove(at: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "comments" {
            let destinationController = segue.destination as! CommentsViewController
            destinationController.selectedItems = self.selectedCells
        }
    }
}
