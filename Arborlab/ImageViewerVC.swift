//
//  ViewController.swift
//  ScrollView
//
//  Created by Freedom on 29/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit

class ImageViewerVC: UIViewController{
    
    var mainScrollView: UIScrollView!
    var imageUrl:String!
    var originX:CGFloat = 0.0
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imagemode = Shared.shared.imageMode {
            if imagemode == "Viewer" {
                if let imageNumber = Shared.shared.imageNumber{
                    if let imageUrl = Shared.shared.imageUrl {
                        self.imageUrl = imageUrl[imageNumber]
                    }
                }
            }
        }
        
        initSizeSetup()
    }
    
    private func initSizeSetup() {
        
        mainScrollView = UIScrollView(frame: view.bounds)
        self.view.addSubview(self.mainScrollView)
        
        let scrollView = UIScrollView(frame: CGRect(x: originX, y: 0.0, width: mainScrollView.bounds.width, height: self.mainScrollView.bounds.height - 40))
        mainScrollView.addSubview(scrollView)
        
        let imageView = UIImageView(frame: scrollView.bounds)
        scrollView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        if let imageUrl = self.imageUrl {
            print(imageUrl)
            imageView.af_setImage(withURL: URL(string: imageUrl)!, placeholderImage: #imageLiteral(resourceName: "AddImage"), filter: nil, progress: { (progress) in
                let progressBar = UIProgressView(frame: CGRect(x: 0, y: self.view.bounds.height - 2, width: self.view.bounds.width, height: 2))
                progressBar.progress = Float(progress.fractionCompleted)
                self.view.addSubview(progressBar)
            }, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: true, completion: nil)
            
            originX = (scrollView.frame.minX + scrollView.frame.width)
            
            scrollView.minimumZoomScale = 1
            scrollView.maximumZoomScale = 3.0
            scrollView.delegate = self
            
            let doubleTap = UITapGestureRecognizer(target: self, action: #selector(ImageViewerVC.handleDoubleTap(_:)))
            doubleTap.numberOfTapsRequired = 2
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(doubleTap)
            mainScrollView.isScrollEnabled = true
            mainScrollView.isPagingEnabled = true
            mainScrollView.showsHorizontalScrollIndicator = true
            mainScrollView.contentSize = CGSize(width: originX, height: mainScrollView.bounds.height)
        }
        
    }
    
    func handleDoubleTap(_ recognizer: UITapGestureRecognizer) {
        let imageview = recognizer.view as! UIImageView
        let scrollview = imageview.superview as! UIScrollView
        
        if scrollview.zoomScale > scrollview.minimumZoomScale {
            scrollview.setZoomScale(scrollview.minimumZoomScale, animated: true)
        } else if scrollview.zoomScale <= scrollview.minimumZoomScale{
            scrollview.setZoomScale(scrollview.maximumZoomScale, animated: true)
        }
    }
    
    func recenterImage(_ scrollView:UIScrollView, imageView:UIImageView) {
        let offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width) ?
            (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0
        let offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height) ?
            (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0
        imageView.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX,y: scrollView.contentSize.height * 0.5 + offsetY)
    }
    
}


extension ImageViewerVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        for imageView in scrollView.subviews {
            if imageView is UIImageView {
                return imageView
            }
        }
        return nil
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageView = scrollView.subviews.first as! UIImageView
        self.recenterImage(scrollView, imageView: imageView)
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scrollView.minimumZoomScale >= scale {
            scrollView.setZoomScale(1.0, animated: true)
        } else if scrollView.maximumZoomScale <= scale {
            scrollView.setZoomScale(3.0, animated: true)
        }
    }
}
