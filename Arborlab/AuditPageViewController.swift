//
//  AuditPageViewController.swift
//  Arborlab
//
//  Created by Ming Fang on 1/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire

class AuditPageViewController: UIViewController, ImagesProtocol {

    var pageViewController: UIPageViewController!
    var auditDetailsPageController : AuditDetailsPageViewController!
    var imageOneController: ImagePageOneCVC!
    var imageTwoController: ImagePageTwoCVC!
    var controllers = [UIViewController]()
    let notificationMsg = NSNotification.Name(rawValue: "currentPageChanged")
    let pageViewMsg = NSNotification.Name(rawValue: "aaImages")
    var imageDic = [URL]()
    
    @IBOutlet weak var sliderView: UIView!
    var sliderImageView: UIImageView!
    var lastPage = 0
    var currentPage: Int = 0 {
        didSet {
            //indicator setup
            let offset = self.view.frame.width / 3.0 * CGFloat(currentPage)
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.sliderImageView.frame.origin = CGPoint(x: offset, y: -1)
            }
            
            //change directions
            if currentPage > lastPage {
                self.pageViewController.setViewControllers([controllers[currentPage]], direction: .forward, animated: true, completion: nil)
            }
            else {
                self.pageViewController.setViewControllers([controllers[currentPage]], direction: .reverse, animated: true, completion: nil)
            }
            
            lastPage = currentPage
        }
    }
    @IBOutlet weak var uploadButton: UIBarButtonItem!
    @IBAction func uploadBtn(_ sender: Any) {
        if let imageCount = Shared.shared.imagesCount {
            if imageCount < 2{
                jumpToUploader()
            } else {
                let alertController = UIAlertController(title: "Images Selection", message: "Do you want to reselct images?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                    (result : UIAlertAction) -> Void in
                    Shared.shared.aaImageRemove = false
                    Shared.shared.imageUrlTwo.removeAll()
                    self.jumpToUploader()
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(imageDic.count)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.aaImageRemove = true
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(red:0.976,  green:0.976,  blue:0.976, alpha:1)
        
        pageViewController = self.childViewControllers.first as! UIPageViewController
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        auditDetailsPageController = storyboard?.instantiateViewController(withIdentifier: "AuditDetailsPage") as! AuditDetailsPageViewController
        imageOneController = storyboard?.instantiateViewController(withIdentifier: "ImagePageOne") as! ImagePageOneCVC
        imageTwoController = storyboard?.instantiateViewController(withIdentifier: "ImagePageTwo") as! ImagePageTwoCVC
        
        //Manually add first view
        pageViewController.setViewControllers([auditDetailsPageController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        
        sliderImageView = UIImageView(frame: CGRect(x: 0, y: -1, width: self.view.frame.width / 3.0, height: 3.0))
        sliderImageView.image = UIImage(named: "Slider")
        sliderView.addSubview(sliderImageView)
        
        controllers.append(auditDetailsPageController)
        controllers.append(imageOneController)
        controllers.append(imageTwoController)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AuditPageViewController.currentPageChanged(notification:)), name: notificationMsg, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ImagePageTwoCVC.imageDeliverNotification(notification:)), name: pageViewMsg, object: nil)
    }
    
    func imageDeliverNotification(notification: NSNotification) {
        if let status = Shared.shared.aaImageRemove {
            if !status {
                imageDic.removeAll()
            }
        }
        let imageUrl:URL = notification.object as! URL
        imageDic.append(imageUrl)
    }
    
    private func jumpToUploader() {
        let detinationController = storyboard?.instantiateViewController(withIdentifier: "ImageUploadVC") as! ImageUploaderVC
        let destinationNav = UINavigationController(rootViewController: detinationController)
        Shared.shared.imageMode = "uploadImageFromAA"
        present(destinationNav, animated: true, completion: nil)
    }
    
    @IBAction func changeCurrentPage(sender: UIButton) {
        currentPage = sender.tag - 100
        setTitle(pageIndex: currentPage)
    }
    
    func currentPageChanged(notification: NSNotification) {
        currentPage = notification.object as! Int
        setTitle(pageIndex: currentPage)
    }
    
    func setTitle(pageIndex: Int) {
        if pageIndex == 0 {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(red:0.976,  green:0.976,  blue:0.976, alpha:1)
            
            self.navigationController!.navigationBar.topItem!.title = "Information"
        }
        if pageIndex == 1 {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(red:0.976,  green:0.976,  blue:0.976, alpha:1)

            self.navigationController!.navigationBar.topItem!.title = "Arb Images"
        }
        if pageIndex == 2 {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0,  green:0.478,  blue:1, alpha:1)

            self.navigationController!.navigationBar.topItem!.title = "AA Images"
        }
    }
    
    //get changed AaStatus,Comments from AuditDetailsPage
    func uploadImageToServer(imagePath: URL, imageNum: String) {
        let api_token:String = UserDefaults.standard.string(forKey: "api_token")!
        
        if let auditResultData = Shared.shared.auditResultData {
            let parameters = [
                "kpi_index" : auditResultData.index,
                "aaStatus" : String(auditResultData.aaStatus),
                "comments" : auditResultData.comments,
                "api_token" : api_token,
                "imageNum": imageNum]
            //[String:String] parameters
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(imagePath, withName: "image")
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
            },
                to: Variables.aaUploadUrl + auditResultData.matchid,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response.result)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            })
            
        }
        
    }
    
    func getImage(image: UIImage, imagePath: URL) {
        imageDic.append(imagePath)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if imageDic.count == 1{
            uploadImageToServer(imagePath: imageDic[0], imageNum: "1")

        }
        if imageDic.count == 2{
            uploadImageToServer(imagePath: imageDic[1], imageNum: "2")
        }
    }
}

extension AuditPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKind(of: AuditDetailsPageViewController.self) {
            return imageOneController
        }
        else if viewController.isKind(of: ImagePageOneCVC.self){
            return imageTwoController
        }
        return nil
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKind(of: ImagePageOneCVC.self) {
            return auditDetailsPageController
        }
        else if viewController.isKind(of: ImagePageTwoCVC.self){
            return imageOneController
        }
        return nil
    }
}
