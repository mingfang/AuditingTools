//
//  LoginVC.swift
//  Arborlab
//
//  Created by Ming Fang on 9/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import RealmSwift
import ReachabilitySwift

class LoginViewController: UIViewController {
    
    let reachability = Reachability()!
    var isLoggedIn:Bool!
    @IBOutlet weak var usernameTxtfield: UITextField!
    @IBOutlet weak var passwordTxtfield: UITextField!
    @IBAction func loginBtn(_ sender: Any) {
        
        if reachability.isReachable {
            if isLoggedIn == true {
                self.performSegue(withIdentifier: "LoginSegue", sender: nil)
            } else {
                loginViaInternet()
            }
        }
    }
    
    func loginViaInternet() {
        var username = usernameTxtfield.text
        var password = passwordTxtfield.text
        
        username = "arborlab"
        password = "password"
        
        let paramLogin = ["username": username! as String,
                          "password": password! as String]
        let progressActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressActivity.label.text = "Login"
        progressActivity.detailsLabel.text = "Writing user data..."
        FetchData.login(url: Variables.loginUrl, parameter: paramLogin, success: { (status, info, api_token, userId) in
            UserDefaults.standard.set(api_token, forKey: "api_token")
            UserDefaults.standard.set(userId, forKey: "userId")
            UserDefaults.standard.set(true, forKey: "login")
            self.performSegue(withIdentifier: "LoginSegue", sender: nil)
            
            progressActivity.detailsLabel.text = info
            progressActivity.hide(animated: true, afterDelay: 1)
        }, fail:{ (error) in
            progressActivity.detailsLabel.text = "Something Wrong!"
            print(error)
            progressActivity.hide(animated: true, afterDelay: 1)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print((Realm.Configuration.defaultConfiguration.fileURL?.absoluteString)! as String)
        
        isLoggedIn = UserDefaults.standard.bool(forKey: "login")
        if isLoggedIn == true {
            self.performSegue(withIdentifier: "LoginSegue", sender: nil)
            print(UserDefaults.standard.string(forKey: "api_token")!)
        }
    }
}
