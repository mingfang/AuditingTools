//
//  AuditData.swift
//  Arborlab
//
//  Created by Ming Fang on 21/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import Foundation
class AuditData {
    var auditID:String
    var parkName:String
    var auditDate:String
    var sorGroup:String
    var sorCode:String
    
    init?(auditID:String, parkName:String, auditDate:String, sorGroup:String, sorCode:String) {
        self.auditID = auditID
        self.auditDate = auditDate
        self.parkName = parkName
        self.sorCode = sorCode
        self.sorGroup = sorGroup
        
        if auditID.isEmpty || auditDate.isEmpty || sorGroup.isEmpty || sorCode.isEmpty || parkName.isEmpty {
            self.auditID = "None"
            self.auditDate = "0000-00-00"
            self.parkName = "None"
            self.auditID = "None"
            self.sorCode = "None"
            self.sorGroup = "None"
        }
    }
}
