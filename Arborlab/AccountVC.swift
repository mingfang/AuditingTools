//
//  AccountVC.swift
//  Arborlab
//
//  Created by Ming Fang on 2/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import RealmSwift

class AccountVC: UITableViewController {
    
    let sectionTitle = ["User Info","Other options"]
    let menuTitles = ["Health&Safety Records","Health&Safety Questions List", "Logout"]
    var username = ""
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserInfo()
        
        tableView.register(UserInfoCell.self, forCellReuseIdentifier: "UserInfoCell")
        tableView.register(UserInfoMenu.self, forCellReuseIdentifier: "AccountCell")
        tableView.separatorStyle = .none
        
        tableView.tableFooterView = UIView()
        
    }
    
    func getUserInfo() {
        if let userId = UserDefaults.standard.string(forKey: "userId") {
            let realm = try! Realm()
            let userFilter = NSPredicate(format: "id = %d", Int(userId)!)
            let userRealm = realm.objects(User.self).filter(userFilter)
            for item in userRealm {
                username = item.username
                email = item.email
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0 {
            rowCount = 1
        } else if section == 1 {
            rowCount = menuTitles.count
        }
        return rowCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell") as! UserInfoCell

            let userInfoTitle = UILabel(frame: CGRect(x: 10, y: 8, width: self.view.bounds.width - 20, height: 30))
            userInfoTitle.font = UIFont.boldSystemFont(ofSize: 24)
            userInfoTitle.text = username
            
            let emailTitle = UILabel(frame: CGRect(x: 10, y: 38, width: self.view.bounds.width - 20, height: 30))
            emailTitle.font = UIFont.boldSystemFont(ofSize: 17)
            emailTitle.text = email
            
            cell.addSubview(emailTitle)
            cell.addSubview(userInfoTitle)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell") as! UserInfoMenu
            cell.contentView.backgroundColor = UIColor.clear
            
            let whileSpacingView: UIView = UIView(frame: CGRect(x: 6, y: 4, width: UIScreen.main.bounds.width - 12, height: 38))
            let menuTitle = UILabel(frame: CGRect(x: 4, y: 4, width: self.view.bounds.width, height: 30))
            menuTitle.text = menuTitles[indexPath.row]
            
            whileSpacingView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
            whileSpacingView.layer.masksToBounds = false
            whileSpacingView.layer.cornerRadius = 2.0
            whileSpacingView.layer.shadowOffset = CGSize(width: -1, height: 1)
            whileSpacingView.layer.shadowOpacity = 0.2
            
            whileSpacingView.addSubview(menuTitle)
            cell.addSubview(whileSpacingView)
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 74
        } else {
            return 44
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                self.performSegue(withIdentifier: "SafetyRecords", sender: nil)
            case 1:
                self.performSegue(withIdentifier: "QuestionList", sender: nil)
            case 2:
                UserDefaults.standard.removeObject(forKey: "api_token")
                UserDefaults.standard.set(false, forKey: "login")
                let loginPage = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                present(loginPage, animated: true, completion: nil)
            default:
                print("table switch default")
            }
        }
    }
}

class UserInfoCell: UITableViewCell {
    override func awakeFromNib() {
    }
}

class UserInfoMenu: UITableViewCell {
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        
        let whileSpacingView: UIView = UIView(frame: CGRect(x: 5, y: 4, width: UIScreen.main.bounds.width - 10, height: 82))
        
        whileSpacingView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whileSpacingView.layer.masksToBounds = false
        whileSpacingView.layer.cornerRadius = 2.0
        whileSpacingView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whileSpacingView.layer.shadowOpacity = 0.2
        
        self.contentView.addSubview(whileSpacingView)
        self.contentView.sendSubview(toBack: whileSpacingView)
    }
}

