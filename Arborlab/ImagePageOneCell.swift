//
//  ImagePageOneCell.swift
//  Arborlab
//
//  Created by Freedom on 28/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit

class ImagePageOneCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
