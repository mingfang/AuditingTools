//
//  FetchData.swift
//  Arborlab
//
//  Created by Ming Fang on 18/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class FetchData {
    
    static func get<T:Object> (parameter: Parameters, type: T.Type, success:@escaping () -> Void, fail:@escaping (_ error:NSError)->Void)->Void where T: Mappable, T:Meta{
        Alamofire.request(type.url(),
                          method: .get,
                          parameters: parameter,
                          encoding: URLEncoding.default,
                          headers: ["Content-Type":"application/json"]).responseArray { (responseData:DataResponse<[T]>) -> Void in
                            switch(responseData.result) {
                            case .success(let items):
                                autoreleasepool {
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            for item in items {
                                                realm.add(item, update: true)
                                            }
                                        }
                                    } catch let error as NSError {
                                        fail(error)
                                    }
                                }
                                success()
                            case .failure(let error):
                                fail(error as NSError)
                            }
                            
        }
    }
    
    static func post(url : String, parameter : Parameters , success:@escaping () -> Void,
                     fail: @escaping (_ error:NSError) -> Void ) -> Void {
        Alamofire.request(url,
                          method: .post,
                          parameters: parameter,
                          encoding: URLEncoding.default).responseJSON{ (respondsData: DataResponse) -> Void in
                            switch (respondsData.result) {
                            case .success(let value):
                                debugPrint(value)
                                print(respondsData.result)
                                success()
                            case .failure(let error):
                                fail(error as NSError)
                            }
        }
    }
    
    static func register(url : String, parameter : Parameters , success:@escaping (_ status:String, _ info:String) -> Void,
                     fail: @escaping (_ error:NSError) -> Void ) -> Void {
        Alamofire.request(url,
                          method: .post,
                          parameters: parameter,
                          encoding: URLEncoding.default,
                          headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON{ (respondsData: DataResponse) -> Void in
                            switch (respondsData.result) {
                            case .success:
                                print(respondsData.result.value!)
                                if let jsonObject = respondsData.result.value as? [String:Any] {
                                    let status = jsonObject["status"] as? String
                                    let info = jsonObject["info"] as? String
                                    success(status!, info!)
                                }
                                
                            case .failure(let error):
                                fail(error as NSError)
                            }
        }
    }
    
    static func login(url : String, parameter : Parameters , success:@escaping (_ status:String, _ info:String, _ api_token:String, _ userId:Int) -> Void,
                     fail: @escaping (_ error:NSError) -> Void ) -> Void {
        Alamofire.request(url,
                          method: .post,
                          parameters: parameter,
                          encoding: URLEncoding.default,
                          headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON{ (respondsData: DataResponse) -> Void in
                            switch (respondsData.result) {
                            case .success:
                                print(respondsData.result.value!)
                                if let jsonObject = respondsData.result.value as? [String:Any] {
                                    let status = jsonObject["status"] as? String
                                    let info = jsonObject["info"] as? String
                                    let userId = jsonObject["id"] as? Int
                                    var api_token = jsonObject["api_token"] as? String
                                    if api_token == nil {
                                        api_token = "0"
                                    }
                                    success(status!, info!, api_token!, userId!)
                                }
                            case .failure(let error):
                                fail(error as NSError)
                            }
        }
    }
    
    static func questions(url: String,
                          success: @escaping (_ questionId:Int, _ questionTitle:String, _ questionSubTitle:String) -> Void,fail: @escaping (_ error:NSError) -> Void) -> Void {
        Alamofire.request(url,
                          method: .post,
                          encoding: URLEncoding.default,
                          headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON{ (respondsData: DataResponse) -> Void in
                            switch (respondsData.result) {
                            case .success:
                                if let jsonObject = respondsData.result.value as? [String:Any] {
                                    let id = jsonObject["hs_id"] as? Int
                                    let title = jsonObject["description"] as? String
                                    let subtitle = jsonObject["details"] as? String
                                    success(id!, title!, subtitle!)
                                }
                            case .failure(let error):
                                fail(error as NSError)
                            }
        }
    }
    
    
    
    static func cleanTempRealmDB() {
        autoreleasepool {
            let realm = try! Realm()
            let object = realm.objects(TempRealm.self)
            if  object.count > 0 {
                try! realm.write {
                    realm.delete(object)
                }
            }
        }
    }
    
    static func numToStatus(num:Int, numConvert:@escaping (_ color:UIColor,_ status:String)->Void ) -> Void{
        var status = ""
        var color:UIColor!
        switch num {
        case 0:
            status = "FAILED"
            color = Variables.Colors.failColor
        case 1:
            status = "PASSED"
            color = Variables.Colors.passColor
        case 3:
            status = "NONE"
            color = UIColor.black
        default:
            status = "default"
        }
        
        numConvert(color, status)
    }
    
    static func spaceRemove(originalUrl:String) -> String {
        return originalUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    
}
