//
//  QuestionListSecHeader.swift
//  Arborlab
//
//  Created by Ming Fang on 14/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit

class QuestionListSecHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var headerText: UILabel!
    
    
}

class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = -4.0
    @IBInspectable var bottomInset: CGFloat = 0
    @IBInspectable var leftInset: CGFloat = 0
    @IBInspectable var rightInset: CGFloat = 0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
