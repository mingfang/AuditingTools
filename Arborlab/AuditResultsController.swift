//
//  AuditResultsController.swift
//  Arborlab
//
//  Created by Ming Fang on 16/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import RealmSwift

class AuditResultsController: UITableViewController {
    
    //MySQL matchId from previous ViewController
    var matchId = ""
    var isViewRemoved = false
    var filterData = [AuditResultsData]()
    let searchController = UISearchController(searchResultsController: nil)
    
    let realm = try! Realm()
    
    //Alamofire Parameters
    var parameter = [String:String]()
    var arrayRespond = [[String:AnyObject]]()
    
    var auditResultData = [AuditResultsData]()
    let msgRefresh = NSNotification.Name(rawValue: "refresh")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadTableData()
        loadSearchBar()
        
        //add title and subtitle
        if let parkname = auditResultData.first?.parkName {
            self.navigationItem.titleView = setTitle(title: parkname, subtitle: "KPI's")
        }
        
        //reload table once the page view changed
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadTableData), name: msgRefresh, object: nil)
    }

    func loadSearchBar() {
        //search bar setup
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "PASS / FAIL"
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false
        
        //Scope Bar
        searchController.searchBar.scopeButtonTitles = ["ARB", "AA"]
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func setTitle(title:String, subtitle:String) -> UIView {
        let titleLabel = UILabel(frame: CGRect(x:0, y:-5, width:0, height:0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x:0, y:18, width:0, height:0))
        subtitleLabel.backgroundColor = UIColor.clear
        subtitleLabel.textColor = UIColor.darkGray
        subtitleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x:0, y:0, width:max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height:30))
        
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width
        
        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }
        
        return titleView
    }
    
    func loadTableData() {
        //Realm
        let allResults = realm.objects(AuditResults.self)
        let byDate = allResults.sorted(byProperty: "id")
        
        auditResultData.removeAll()
        for result in byDate {
            if result.matchId == matchId {
                let auditResultDataRow = AuditResultsData(auditID: result.id, parkName: result.parkname, auditDate: result.date, modifiedDate: result.modifiedDate, sorGroup: result.sorgrouping, sorCode: result.sorcode, arbStatus: result.arbStatus, aaStatus: result.aaStatus, comments: result.comments, assessment: result.assessment, index: result.index, matchid: result.matchId, photo1:result.photo1, photo2:result.photo2, aaPhoto1: result.aaPhoto1, aaPhoto2: result.aaPhoto2, id: result.id, tracker: result.tracker)
                auditResultData.append(auditResultDataRow!)
            }
        }
        reloadTableView(self.tableView)
    }
    
    func reloadTableView(_ tableView: UITableView) {
        let contentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(contentOffset, animated: false)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filterData.count
        }
        return auditResultData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuditResultsCell", for: indexPath) as! AuditResultsCell
        
        let row : AuditResultsData
        if searchController.isActive && searchController.searchBar.text != "" {
            row = filterData[indexPath.row]
        } else {
            row = auditResultData[indexPath.row]
        }
        
        if indexPath.row == 0 {
            self.tableView.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: UITableViewScrollPosition.top, animated: false)
        }
        
        let arbStatus = row.arbStatus
        let aaStatus = row.aaStatus
        
        cell.indexLabel.text = row.index
        cell.arbStatus.text = String(arbStatus)
        cell.aaStatus.text = String(aaStatus)
        cell.assessmentBriefLabel.text = row.assessment
        cell.dateLabel.text = row.auditDate
        
        FetchData.numToStatus(num: aaStatus) { (color, status) in
            cell.aaStatus.textColor = color
            cell.aaStatus.text = status
        }
        
        FetchData.numToStatus(num: arbStatus) { (color, status) in
            cell.arbStatus.textColor = color
            cell.arbStatus.text = status
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedResult = auditResultData[indexPath.row]
        Shared.shared.auditResultData = selectedResult
    }
    
    func spaceRemove(originalUrl:String) ->String {
        return originalUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    //SearchBar Filter
    func filterContentForSearchText(_ searchText: String, scope: Int) {
        filterData = auditResultData.filter({( filtedData : AuditResultsData) -> Bool in
            var searchField = ""
            
            switch (scope) {
            case 0:
                searchField = String(filtedData.aaStatus)
            case 1:
                searchField = String(filtedData.arbStatus)
            default:
                searchField = ""
            }
            return searchField.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        if isViewRemoved == false {
            self.tableView.removeFromSuperview()
        }
    }
}

extension AuditResultsController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.selectedScopeButtonIndex)
    }
}

extension AuditResultsController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.selectedScopeButtonIndex
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}
