//
//  MainTabViewController.swift
//  Arborlab
//
//  Created by Ming Fang on 1/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import MBProgressHUD

class MainTabViewController: UITabBarController, UITabBarControllerDelegate{
    
    var viewControllerOne:ValidWorkViewController!
    var viewControllerTwo:ValidWorkViewController!
    var viewControllerThree:AccountVC!
    var controllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    func initSetup() {
        
        delegate = self
        UINavigationBar.appearance().tintColor = Variables.Colors.primaryColor
        UITabBar.appearance().tintColor = Variables.Colors.passColor
        UITabBar.appearance().isTranslucent = false
        
        viewControllerOne = storyboard?.instantiateViewController(withIdentifier: "ContractorResults") as! ValidWorkViewController
        viewControllerTwo = storyboard?.instantiateViewController(withIdentifier: "ContractorResults") as! ValidWorkViewController
        viewControllerThree = storyboard?.instantiateViewController(withIdentifier: "Account") as! AccountVC
        
        let navOne = UINavigationController(rootViewController: viewControllerOne)
        let navTwo = UINavigationController(rootViewController: viewControllerTwo)
        let navThree = UINavigationController(rootViewController: viewControllerThree)
        navOne.navigationBar.topItem?.title = "Review"
        navTwo.navigationBar.topItem?.title = "New Audit"
        navThree.navigationBar.topItem?.title = "Account"
        controllers.append(navOne)
        controllers.append(navTwo)
        controllers.append(navThree)
        
        //tab items setup
        viewControllerOne.tabBarItem = UITabBarItem(title: "Review", image: #imageLiteral(resourceName: "TabHistory"), selectedImage: nil)
        viewControllerTwo.tabBarItem = UITabBarItem(title: "Audit", image: #imageLiteral(resourceName: "TabAudit"), selectedImage: nil)
        viewControllerThree.tabBarItem = UITabBarItem(title: "Health&Safety", image: #imageLiteral(resourceName: "TabUser"), selectedImage: nil)
        
        self.viewControllers = controllers
        self.selectedViewController = navTwo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Never lose this function again...
        super.viewWillAppear(false)
        //Never lose this function again...

        tabBar.barTintColor = Variables.Colors.superLightColor
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}
