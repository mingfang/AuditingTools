//
//  ValidWorkViewController.swift
//  Arborlab
//
//  Created by Ming Fang on 15/11/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import RealmSwift
import ObjectMapper

class ValidWorkViewController: UITableViewController {
    
    //property
    var filterData = [AuditData]()
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet var validWorkView: UITableView!
    
    var days = 5
    var parameter = [String:String]()
    var auditData = [AuditData]()
    var validAuditData = [AuditData]()
    var auditResult = [AuditResultsData]()
    var cellColorBackgrounds = [UIColor]()
    
    var api_token = ""
    var userId = 0

    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //init Setup
        self.navigationController?.navigationBar.backgroundColor = Variables.Colors.superLightColor
        self.navigationController?.navigationBar.isTranslucent = false

        //remove emtpy cell
        tableView.tableFooterView = UIView()
        
        //searchbar
        loadSearchBar()
        loadTableData()
        
        Shared.shared.isUpdate = true
        if let status = Shared.shared.isUpdate {
            if status {
                updateData()
                loadTableData()
                loadSearchBar()
                Shared.shared.isUpdate = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let value = Shared.shared.selectedDays {
            days = value
        }
        
        if let status = Shared.shared.isUpdate {
            if status == false {
                loadSearchBar()
                loadTableData()
            }
        }
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {}
    @IBAction func refresh(_ sender: Any) {
        updateData()
    }
    
    //update Contractor Audit User data to the local database
    func updateData() {
        
        let api_token:String = UserDefaults.standard.string(forKey: "api_token")!
        
        let loadingActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingActivity.label.text = "Database Updates"
        loadingActivity.detailsLabel.text = "Requesting data from database"
        
        FetchData.get(parameter: ["api_token": api_token], type: ContractorResults.self, success: {
            loadingActivity.detailsLabel.text = "Contractor Results is updating"
            loadingActivity.hide(animated: true, afterDelay: 0.5)
        }, fail: { (error) in
            print("ContractorResults")
            print(error.debugDescription)
        })
        FetchData.get(parameter: ["api_token": api_token], type: AuditResults.self, success: {
            loadingActivity.detailsLabel.text = "Audit Results is updating"
            loadingActivity.hide(animated: true, afterDelay: 0.5)
        }, fail: { (error) in
            print("AuditResults")
            print(error.debugDescription)
        })
        FetchData.get(parameter: ["api_token": api_token], type: User.self, success: {
            loadingActivity.detailsLabel.text = "User Results is updating"
            loadingActivity.hide(animated: true, afterDelay: 0.5)
        }, fail: { (error) in
            print("User")
            print(error.debugDescription)
        })
        FetchData.get(parameter: ["api_token": api_token], type: Questions.self, success: {
            loadingActivity.detailsLabel.text = "Questions Results is updating"
            loadingActivity.hide(animated: true, afterDelay: 0.5)
        }, fail: { (error) in
            print("Questions")
            print(error.debugDescription)
        })
        FetchData.get(parameter: ["api_token": api_token], type: HealthSafetyHistory.self, success: {
            loadingActivity.detailsLabel.text = "HealthSafetyHistory Results is updating"
            loadingActivity.hide(animated: true, afterDelay: 0.5)
        }, fail: { (error) in
            print("Questions")
            print(error.debugDescription)
        })
    }
    
    //load Seachbar setting
    func loadSearchBar() {
        
        //Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        //Scope Bar
        searchController.searchBar.scopeButtonTitles = ["SORGROUP", "SORCODE", "PARKNAME", "DATE"]
        tableView.tableHeaderView = searchController.searchBar
    }
    
    //reload Table Data
    func loadTableData() {
        if navigationItem.title == "Review" {
            validAuditData.removeAll()
            for count in (0...days-1).reversed() {
                let predicate = NSPredicate(format: "aaStatus != %d && date == %@ && comments != %@", 3, getDate(num: days-count).toString(), "")
                let reviewedResults = realm.objects(AuditResults.self).filter(predicate)
                if reviewedResults.count != 0 {
                    for result in reviewedResults {
                        let reviewSearch = NSPredicate(format: "id = %@", result.matchId)
                        let searchResult = realm.objects(ContractorResults.self).filter(reviewSearch)
                        
                        let oneOfDatesInRange = getDate(num: days-count).toString()
                        for searchRow in searchResult {
                            if searchRow.date == oneOfDatesInRange {
                                let auditDataRow = AuditData(auditID: searchRow.id, parkName: searchRow.asset, auditDate: searchRow.date, sorGroup: searchRow.sorgrouping, sorCode: searchRow.sorcode)
                                validAuditData.append(auditDataRow!)
                            }
                        }
                    }
                }
            }
        } else if navigationItem.title == "New Audit" {
            let allResults = realm.objects(ContractorResults.self)
            validAuditData.removeAll()
            cellColorBackgrounds.removeAll()
            for count in (0...days-1).reversed() {
                let oneOfDatesInRange = getDate(num: days-count).toString()
                for result in allResults {
                    if result.date == oneOfDatesInRange {
                        let auditDataRow = AuditData(auditID: result.id, parkName: result.asset, auditDate: result.date, sorGroup: result.sorgrouping, sorCode: result.sorcode)
                        validAuditData.append(auditDataRow!)
                    }
                }
                for validItem in validAuditData {
                    let validMatchId = validItem.auditID
                    let auditPredicate = NSPredicate(format: "matchId == %@ && date == %@", validMatchId, getDate(num: days-count).toString())
                    let arbResult = realm.objects(AuditResults.self).filter(auditPredicate)
                    var isPassColor = true
                    for realmItem in arbResult {
                        if realmItem.arbStatus == 0 {
                            isPassColor = false
                        }
                    }
                    if isPassColor{
                        cellColorBackgrounds.append(Variables.Colors.cellBackGreenColor)
                    } else {
                        cellColorBackgrounds.append(Variables.Colors.cellBackRedColor)
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tableView.removeFromSuperview()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filterData.count
        }
        return validAuditData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValidWorkCell", for: indexPath) as! WorksCell
        let cellData: AuditData
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cellData = filterData[indexPath.row]
        } else {
            cellData = validAuditData[indexPath.row]
        }
        
        cell.dateLabel.text =  cellData.auditDate
        cell.parkNameLabel.text = cellData.parkName
        cell.sorcodeLabel.text = cellData.sorCode
        cell.sorgroupLabel.text = cellData.sorGroup

        //set background red if it have false
        if navigationItem.title == "New Audit" {
            cell.layer.backgroundColor = cellColorBackgrounds[indexPath.row].cgColor
        }
        
        //show "No data" status in view
        if validAuditData.count == 0 {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            label.text = "Please reselect the date."
            label.textColor = UIColor.black
            label.textAlignment = .center
            label.sizeToFit()
            tableView.backgroundView = label
            tableView.separatorStyle = .none
        }
        
        //hide searchbar when cell is full.
        if indexPath.row == 0 {
            self.tableView.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: UITableViewScrollPosition.top, animated: false)
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "ShowAuditResults" {
            let auditResultView = segue.destination as? AuditResultsController
            if let selectedCell = sender as? WorksCell {
                let indexPath = tableView.indexPath(for: selectedCell)!
                let selectedResults = validAuditData[indexPath.row]
                auditResultView!.matchId = selectedResults.auditID
            }
        }
    }
    
    //get the date for the date range selection
    func getDate(num:Int) -> Date {
        let calendar = Calendar.current
        let date:Date?
        
        switch num {
        case 1:
            date = calendar.date(byAdding: .day, value: -110, to: Date())
        case 2:
            date = calendar.date(byAdding: .day, value: -111, to: Date())
        case 3:
            date = calendar.date(byAdding: .day, value: -112, to: Date())
        case 4:
            date = calendar.date(byAdding: .day, value: -113, to: Date())
        case 5:
            date = calendar.date(byAdding: .day, value: -114, to: Date())
        default:
            date = calendar.date(byAdding: .day, value: 0, to: Date())
        }
        return date!
    }
    
    //filter of the search bar
    func filterContentForSearchText(_ searchText: String, scope: Int) {
        filterData = validAuditData.filter({( filtedData : AuditData) -> Bool in
            var searchField = ""
            
            switch (scope) {
            case 0:
                searchField = filtedData.sorGroup
            case 1:
                searchField = filtedData.sorCode
            case 2:
                searchField = filtedData.parkName
            case 3:
                searchField = filtedData.auditDate
            default:
                searchField = ""
            }
            return searchField.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

//delegate searchbar and scope field
extension ValidWorkViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.selectedScopeButtonIndex)
    }
}
extension ValidWorkViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.selectedScopeButtonIndex
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}

//additional date feature of comparison
extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays) as NSDate
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours) as NSDate
        
        //Return Result
        return dateWithHoursAdded
    }
}

//String to date
extension String {
    func toDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self)!
    }
}

