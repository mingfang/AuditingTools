//
//  ShareData.swift
//  Arborlab
//
//  Created by Ming Fang on 2/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import Foundation

final class Shared {
    static let shared = Shared() //lazy init, and it only runs once
    
    var stringValue : String!
    var auditResultData : AuditResultsData!
    var boolValue   : Bool!
    var selectedDays : Int!
    var parkname : String!
    var api_token : String!
    var userId : Int!
    var imageNumber : Int!
    var imageUrl: [String]!
    var imageUrlTwo: [URL]!
    var imageMode: String!
    var imagePath: String!
    var currentLocation: String!
    var imagesCount: Int!
    var isUpdate: Bool!
    var aaImageRemove: Bool!
    var aaImageUrl: URL!
}
