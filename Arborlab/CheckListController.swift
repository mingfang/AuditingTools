//
//  CheckListController.swift
//  Arborlab
//
//  Created by Freedom on 5/12/16.
//  Copyright © 2016 Freedom. All rights reserved.
//

import UIKit

class CheckListController: UITableViewController {

    let questionTitle = ["Questions","Comments & images"]
    let questionDetail = [["detail One","detail Two","detail Three","detail Four"], ["detail One","detail Two","detail Three","detail Four"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Health&Safety Checklist"

        navigationController?.setToolbarHidden(false, animated: true)
        self.tableView.allowsMultipleSelectionDuringEditing = true
        self.tableView.setEditing(true, animated: false)
    }

    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {}
    @IBAction func doneBtn(_ sender: Any) {
        if Shared.shared.parkname == nil {
            Shared.shared.parkname = "None"
        }
        print(Shared.shared.parkname)
        self.performSegue(withIdentifier: "unwindSegue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return questionTitle.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return questionDetail[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistCell", for: indexPath) as! CheckListCell
        
        cell.title.text = self.questionDetail[indexPath.section][indexPath.row]
        
        if indexPath.section == 1 {
            cell.selectionStyle = UITableViewCellSelectionStyle.blue
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return questionTitle[section]
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
