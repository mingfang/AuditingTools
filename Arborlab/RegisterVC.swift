//
//  RegisterVC.swift
//  Arborlab
//
//  Created by Ming Fang on 9/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
class RegisterVC: UIViewController {

    var paramLogin:Parameters!
    
    @IBOutlet weak var usernameTxtField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBAction func registerBtn(_ sender: Any) {
        let username = usernameTxtField.text
        let password = passwordField.text
        let email = emailField.text
        
        paramLogin = ["username": username! as String,
                      "password": password! as String,
                      "email": email! as String]
        let progressActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressActivity.label.text = "Register"
        progressActivity.detailsLabel.text = "Writing user data..."
        FetchData.register(url: Variables.registerUrl, parameter: paramLogin, success: { (status, info) in
            if status == "1" {
                progressActivity.detailsLabel.text = info
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                self.present(destinationController, animated: true, completion: nil)
                
            }
            progressActivity.detailsLabel.text = info
            progressActivity.hide(animated: true, afterDelay: 1)
        }, fail:{ (error) in
            progressActivity.detailsLabel.text = error.domain
            progressActivity.hide(animated: true, afterDelay: 1)
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
