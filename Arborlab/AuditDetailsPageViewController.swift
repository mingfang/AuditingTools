//
//  AuditDetailsPageViewController.swift
//  Arborlab
//
//  Created by Ming Fang on 1/12/16.
//  Copyright © 2016 Ming Fang. All rights reserved.
//

import UIKit
import ReachabilitySwift
import MBProgressHUD
import Alamofire
import RealmSwift

class AuditDetailsPageViewController: UIViewController {
    
    //notification : Page
    let notificationMsg = NSNotification.Name(rawValue: "currentPageChanged")
    //notification : autoRefresh
    let msgRefresh = NSNotification.Name(rawValue: "refresh")
    var loadingActivity:MBProgressHUD?
    let reachability = Reachability()!
    var networkIsAvailable = false
    var auditResultData : AuditResultsData?
    var aaStatusNum = 3
    var receivedImages = [UIImage]()
    var commentImagePath: URL!
    var imageDic = [URL]()
    var isCommentsNone = true
    
    //Interface elements
    @IBOutlet weak var modifiedDate: UILabel!
    @IBOutlet weak var aaBorder: UIView!
    @IBOutlet weak var arbBorder: UIView!
    @IBOutlet weak var parkname: UILabel!
    @IBOutlet weak var sorcode: UILabel!
    @IBOutlet weak var sorgroup: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var arbStatus: UILabel!
    @IBOutlet weak var assessment: UITextView!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var dropDown: DropMenuButton!

    
    deinit {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initVariables()
        setupBorder()
        setupDropDown()
        setupAssessment()
        setupArbStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initVariables()
        
        //post page number to page view controller
        NotificationCenter.default.post(name: notificationMsg, object: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    private func initVariables() {
        //get value from AuditResultTableController
        if let value = Shared.shared.auditResultData {
            auditResultData = value as AuditResultsData
        }
        
        DispatchQueue.main.async {
            //setup comments
            if (self.auditResultData?.comments.isEmpty)! {
                self.setupAuditAuditComments()
            }
        }
        
        
        let mDate = auditResultData!.modifiedDate
        modifiedDate.text = mDate == "" ? "....-..-.." : mDate.substring(to: mDate.index(mDate.startIndex, offsetBy: 10))
        parkname.text = auditResultData!.parkName
        date.text = auditResultData!.auditDate
        sorcode.text = auditResultData!.sorCode
        sorgroup.text = auditResultData!.sorGroup
        comments.text = auditResultData!.comments
        comments.textColor = UIColor.black
        index.text = auditResultData!.index
        
        
        locationChecker(auditResultData: auditResultData!)
        
        UserDefaults.standard.set(auditResultData!.parkName, forKey: "CurrentLocation")
    }
    
    private func checkChanges() -> Bool{
        if auditResultData!.comments != comments.text || auditResultData!.aaStatus != aaStatusNum{
            return true
        }
        return false
    }
    
    func locationChecker(auditResultData: AuditResultsData) {
        if let location = UserDefaults.standard.value(forKey: "CurrentLocation") {
            let parkname = auditResultData.parkName
            if location as! String != parkname as String {
                let destinationController = storyboard?.instantiateViewController(withIdentifier: "QuestionListVC") as! QuestionListVC
                let navController = UINavigationController(rootViewController: destinationController)
                present(navController, animated: true, completion: nil)
            }
            
        }
    }
    
    private func setupArbStatus() {
        FetchData.numToStatus(num: auditResultData!.arbStatus) { (color, status) in
            self.arbStatus.text = status
            self.arbStatus.layer.borderColor = color.cgColor
            self.arbStatus.textColor = UIColor.white
            self.arbStatus.backgroundColor = color
        }
        arbStatus.layer.borderWidth = 2
        arbStatus.layer.masksToBounds = true
        arbStatus.layer.cornerRadius = 4
    }
    
    private func setupAssessment() {
        assessment.text = auditResultData!.assessment
        assessment.layer.backgroundColor = UIColor.clear.cgColor
        assessment.textContainer.lineFragmentPadding = 0
        assessment.textContainerInset = UIEdgeInsets.zero
    }
    
    private func setupDropDown() {
        dropDown.layer.borderColor = UIColor.lightGray.cgColor
        dropDown.initMenu(["PASS","FAIL"], actions: [({ () -> Void in
            self.aaStatusNum = 1
            self.dropDown.backgroundColor = Variables.Colors.passColor
        }),({ () -> Void in
            self.aaStatusNum = 0
            self.dropDown.backgroundColor = Variables.Colors.failColor
        })])
        dropDown.layer.borderWidth = 2
        dropDown.layer.masksToBounds = true
        dropDown.layer.cornerRadius = 4
        
        if auditResultData!.aaStatus == 1 {
            aaStatusNum = 1
            dropDown.setTitle("PASS", for: .normal)
            auditResultData!.aaStatus = aaStatusNum
            self.dropDown.backgroundColor = Variables.Colors.passColor
        }
        
        if auditResultData!.aaStatus == 0 {
            aaStatusNum = 0
            auditResultData!.aaStatus = aaStatusNum
            dropDown.setTitle("FAIL", for: .normal)
            self.dropDown.backgroundColor = Variables.Colors.failColor
        }
        
    }
    
    private func setupAuditAuditComments() {
        comments.text = "Click to input your comments."
        comments.textColor = UIColor.lightGray
        comments.layer.borderColor = UIColor.lightGray.cgColor
        comments.layer.borderWidth = 2
    }
    
    private func setupBorder() {
        self.view.backgroundColor = UIColor.clear
        
        arbBorder.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        arbBorder.layer.masksToBounds = false
        arbBorder.layer.cornerRadius = 2.0
        arbBorder.layer.shadowOffset = CGSize(width: -1, height: 1)
        arbBorder.layer.shadowOpacity = 0.2
        
        aaBorder.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        aaBorder.layer.masksToBounds = false
        aaBorder.layer.cornerRadius = 2.0
        aaBorder.layer.shadowOffset = CGSize(width: -1, height: 1)
        aaBorder.layer.shadowOpacity = 0.2
    }
    
    func reachabilityChanged(_ note: NSNotification) {
        let reachability = note.object as! Reachability
        
        DispatchQueue.main.async {
            self.loadingActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.loadingActivity?.label.text = "Network Status"
            
            if reachability.isReachable {
                self.loadingActivity?.detailsLabel.text = "Internet connected."
                
                if reachability.isReachableViaWiFi {
                    self.loadingActivity?.detailsLabel.text = "Internet connect to Wifi."
                } else {
                    self.loadingActivity?.detailsLabel.text = "Internet connect to Cellular."
                }
                self.networkIsAvailable = true
                
                //post tempRealm data to the remote database
                let realm = try! Realm()
                let tempObject = realm.objects(TempRealm.self)
                for item in tempObject {
                    self.postDataUseAlamofire(comments: item.comments, aaStatus: item.aaStatus)
                }
                FetchData.cleanTempRealmDB()
                self.loadingActivity?.hide(animated: true, afterDelay: 0.5)
            } else {
                self.loadingActivity?.detailsLabel.text = "Internet disconnected. Saving locally now."
                self.loadingActivity?.hide(animated: true, afterDelay: 2)
                self.networkIsAvailable = false
            }
        }
    }
    
    func postDataOnline() {
        if auditResultData?.matchid.isEmpty == false {
            postDataUseAlamofire(comments: comments.text, aaStatus: aaStatusNum)
            saveDataOffline()
        }
    }
    
    func postDataUseAlamofire(comments: String, aaStatus: Int) {
        let api_token:String = UserDefaults.standard.string(forKey: "api_token")!
        let postParameters : Parameters = ["index" : auditResultData!.index,
                                           "aaStatus" : aaStatus,
                                           "comments" : comments,
                                           "api_token" : api_token]
        let postUrl = Variables.aaUploadUrl + auditResultData!.matchid
        FetchData.post(url: postUrl, parameter: postParameters, success: {
            print("Data posted.")
            
        }) { (error) in
            print("Something wrong with posting data.")
            print(error)
        }
    }
    
    func saveDataOffline() {
        let realm = try! Realm()
        let auditResultRealm = AuditResults()
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let todayDate = dateformatter.string(from: Date())
        
        auditResultRealm.arbStatus = auditResultData!.arbStatus
        auditResultRealm.matchId = auditResultData!.matchid
        auditResultRealm.assessment = auditResultData!.assessment
        auditResultRealm.date = auditResultData!.auditDate
        auditResultRealm.index = auditResultData!.index
        auditResultRealm.sorcode = auditResultData!.sorCode
        auditResultRealm.sorgrouping = auditResultData!.sorGroup
        auditResultRealm.parkname = auditResultData!.parkName
        auditResultRealm.id = auditResultData!.id
        auditResultRealm.photo1 = auditResultData!.photo1
        auditResultRealm.photo2 = auditResultData!.photo2
        auditResultRealm.aaPhoto1 = auditResultData!.aaPhoto1
        auditResultRealm.aaPhoto2 = auditResultData!.aaPhoto2
        try! realm.write {
            auditResultRealm.modifiedDate = todayDate
            auditResultRealm.aaStatus = aaStatusNum
            auditResultRealm.comments = comments.text
            realm.add(auditResultRealm, update: true)
        }
        NotificationCenter.default.post(name: msgRefresh , object: nil)
    }
    
    func saveTempData() {
        let realm = try! Realm()
        let tempRealm = TempRealm()
        
        tempRealm.arbStatus = auditResultData!.arbStatus
        tempRealm.matchId = auditResultData!.matchid
        tempRealm.assessment = auditResultData!.assessment
        tempRealm.date = auditResultData!.auditDate
        tempRealm.index = auditResultData!.index
        tempRealm.sorcode = auditResultData!.sorCode
        tempRealm.sorgrouping = auditResultData!.sorGroup
        tempRealm.parkname = auditResultData!.parkName
        tempRealm.id = auditResultData!.id
        tempRealm.photo1 = auditResultData!.photo1
        tempRealm.photo2 = auditResultData!.photo2
        tempRealm.aaPhoto1 = auditResultData!.aaPhoto1
        tempRealm.aaPhoto2 = auditResultData!.aaPhoto2
        try! realm.write {
            tempRealm.aaStatus = aaStatusNum
            tempRealm.comments = comments.text
            realm.add(tempRealm, update: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if checkChanges() {
            print("Input Changed.")
            if networkIsAvailable{
                postDataOnline()
                print("Data Post Online")
            } else {
                saveTempData()
                print("Data Saved OFFLINE temporarily")

                saveDataOffline()
                print("Data Saved OFFLINE")
            }
            auditResultData?.aaStatus = aaStatusNum
            auditResultData?.comments = comments.text
            Shared.shared.auditResultData = auditResultData
        }
    }

}

extension AuditDetailsPageViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Click to input your comments."
            textView.textColor = UIColor.lightGray
        }
    }
    
}
